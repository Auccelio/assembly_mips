################################################################################################################################
#		 Aluno: Augusto Lima Jardim
# 		NUSP: 11810918
#
# 		Aluno: Alx�lio Ribeiro Esteves
# 		NUSP: 11796980
#
################################################################################################################################

.data
	
	myArray: 
		.word 0, 0, 0, 0
		
	PI: .float 3.141592
	
	zero: .float 0.0
	
	um: .float 1.0
	
	msg: .asciiz "Entre com o vetor (X, Y) e o ângulo em graus nessa ordem. Exemplo: 5 15 30 \n"
	
	space: .asciiz " "
	
	msg2: .asciiz "O vetor rotacionado é V = { "
	
	msg3: .asciiz " } "
		
		
.text

##############################################################################################################################
#
#		 Supondo que os ângulos estão entre o intervalo 0 e 2PI #
#
##############################################################################################################################


	j inicio
	
	
	inicio:
	
		#Impressão na tela
		li $v0, 4
		la $a0, msg
		syscall

		#Alocação do array em $s7
		la $s7, myArray
	
		addi $t0, $zero, 0
		addi $t1, $zero, 1

		#Solicito os dados e os gravo nos registradores $sx
		li $v0, 5
		syscall
		move $s0, $v0
		sw $s0, myArray($t0) #aqui a posição está em 0 bytes
		sll $t0, $t1, 2	
			
		li $v0, 5
		syscall
		move $s1, $v0
		sw $s1, myArray($t0) #aqui a posição já está em 4 bytes
	
		li $v0, 5
		syscall
		move $s2, $v0 #guarda o ângulo como inteiro
		jal transforma_quadrante
		jal conversao_angulo
		j fatorial_seno
	
	
	transforma_quadrante:
		
		bgt $s2, 90, quad_234 	#se for maior que 90, pode ser dos quadrantes 2, 3 ou 4
		bgt $s2, 180, quad_34	#se for maior que 180, pode ser dos quadrantes 3 ou 4
		bgt $s2, 270, quad_4  	#se for maior que 270, s� pode ser do quadrante 4
		jr $ra
		
	quad_234:
		blt $s2, 180, quad_2	#se for menor que 180, est� no quadrante 2
		blt $s2, 270, quad_3 	#se for maior que 180 e menor que 270, est� no quadrante 3
		j quad_4 	     	#caso contr�rio, est� no quadrante 4
	
	quad_34:
		blt $s2, 270, quad_3 	#se for maior que 180 e menor que 270, est� no quadrante 3
		j quad_4 	     	#caso contr�rio, est� no quadrante 4
		
	quad_2:
		addi $t3, $zero, 180	#$t3 = 180
		sub $s2 $t3, $s2	#theta = 180 - theta. - vai pro primeiro quadrante. Ex: theta = 120; theta = 180 - 120 => theta = 60
		addi $t2, $zero, 2	# coloca 2 no registrador $t2 para indicar que o �ngulo estava no 2� quadrante e para consertar os sinais de seno e cosseno mais pra frente
		jr $ra
	
	quad_3:
		subi $s2, $s2, 180	#theta = theta - 180. - vai pro primeiro quadrante. Ex: theta = 225; theta = 225 - 180 => theta = 45
		addi $t2, $zero, 3	# coloca 3 no registrador $t2 para indicar que o �ngulo estava no 3� quadrante e para consertar os sinais de seno e cosseno mais pra frente
		jr $ra
	
	quad_4:
		addi $t3, $zero, 360	#$t3 = 360
		sub $s2, $t3, $s2 	#theta = theta - 270. - vai pro primeiro quadrante. Ex: theta = 330; theta = 330 - 270 => theta = 30
		addi $t2, $zero, 4	# coloca 4 no registrador $t2 para indicar que o �ngulo estava no 4� quadrante e para consertar os sinais de seno e cosseno mais pra frente
		jr $ra
		
		
		
	
	conversao_angulo:
	
		lwc1 $f10, PI #carrego da memória para o registrador $t5
		#mtc1 $t5, $f10 #carrego do registrador comum para o registrador float - coprocessor1
		#cvt.s.w $f10, $f10 #converto de inteiro para float
		
		addi $t6, $zero, 180 #carrego 180 para o registrador $t6
		mtc1 $t6, $f11 #carrego do registrador comum para o registrador float - coprocessor1
		cvt.s.w $f11, $f11 #converto de inteiro para float
		
		mtc1 $s2, $f1 #transfere para $f1
		cvt.s.w $f1, $f1 #converte de inteiro para float
		mul.s $f3, $f1, $f10 #$f3 = multiplicação $f1 x PI
		div.s $f4, $f3, $f11 #f4 = divisão $f3 / 180
		jr $ra 
	
	
	fatorial_seno:
		
	addi $t1 , $zero , 1 #atribuo o valor 1 a $t1 - j=1
	addi $t4 , $zero , 1 #atribuo o valor 1 a $t4 - i=1  
	# for ( i =1; i <= n ; i ++)
	for: 
		slti $t7 , $t4 , 14 #n < 14 fat (13) - seta o registrador $t7 enquanto for menor que 14
		beq $t7 , $zero , conversao_fatorial_seno #t7 == false -> fim do for
		mult $t1 , $t4 #resultado em 64 bits ( HI & LO ) - j = j * i ;
		mflo $t1 #move o valor de low register - seleciona 32 bits menos sign
		
		beq $t4, 3, fator3
		volta3:
		beq $t4, 5, fator5
		volta5:
		beq $t4, 7, fator7
		volta7:
		beq $t4, 9, fator9
		volta9:
		beq $t4, 11, fator11
		volta11:
		beq $t4, 13, fator13
		volta13:
		
		addi $t4 , $t4 , 1 #soma-se 1 ao registrador $t4 - i = i + 1
		j for
	
	
	
	fator3:
		add $s2, $zero, $t1
		j volta3
	
	fator5:
		add $s3, $zero, $t1
		j volta5
	
	fator7:
		add $s4, $zero, $t1
		j volta7
	
	fator9:
		add $s5, $zero, $t1
		j volta9

	fator11:
		add $s6, $zero, $t1
		j volta11
		
	fator13:
		add $s7, $zero, $t1
		j volta13
	
		
	
	
	
	conversao_fatorial_seno:
	
		mtc1 $s2, $f26 #transfere para $f26
		cvt.s.w $f26, $f26 #converte de inteiro para float
	
		mtc1 $s3, $f27 #transfere para $f26
		cvt.s.w $f27, $f27 #converte de inteiro para float
		
		mtc1 $s4, $f28 #transfere para $f26
		cvt.s.w $f28, $f28 #converte de inteiro para float
		
		mtc1 $s5, $f29 #transfere para $f26
		cvt.s.w $f29, $f29 #converte de inteiro para float
		
		mtc1 $s6, $f30 #transfere para $f26
		cvt.s.w $f30, $f30 #converte de inteiro para float
		
		mtc1 $s7, $f31 #transfere para $f26
		cvt.s.w $f31, $f31 #converte de inteiro para float
		jal seno
		jal fatorial_cosseno
		j rotaciona
		
		
		
	seno:
	
		lwc1 $f1, zero
		add.s $f20, $f1, $f4 #primeiro fator da Série de Taylor
		
		mul.s $f21, $f20, $f20
		mul.s $f21, $f21, $f20 #segundo fator da Série de Taylor (antes da divisão pelo fatorial)
		add.s $f22, $f1, $f21 #guardo o valor para prosseguir com a exponenciação
		div.s $f21, $f21, $f26 #segundo fator da Série de Taylor
		sub.s $f21, $f1, $f21 #inverter o valor devido a disposição da Série de Taylor
		
		mul.s $f22, $f22, $f20
		mul.s $f22, $f22, $f20 #terceiro fator da Série de Taylor (antes da divisão pelo fatorial)
		add.s $f23, $f1, $f22 #guardo o valor para prosseguir com a exponenciação
		div.s $f22, $f22, $f27 #terceiro fator da Série de Taylor
		
		mul.s $f23, $f23, $f20
		mul.s $f23, $f23, $f20 #quarto fator da Série de Taylor (antes da divisão pelo fatorial)
		add.s $f24, $f1, $f23 #guardo o valor para prosseguir com a exponenciação
		div.s $f23, $f23, $f28 #quarto fator da Série de Taylor
		sub.s $f23, $f1, $f23 #inverter o valor devido a disposição da Série de Taylor
		
		mul.s $f24, $f24, $f20
		mul.s $f24, $f24, $f20 #quinto fator da Série de Taylor (antes da divisão pelo fatorial)
		add.s $f25, $f1, $f24 #guardo o valor para prosseguir com a exponenciação
		div.s $f24, $f24, $f29 #quinto fator da Série de Taylor
		
		mul.s $f25, $f25, $f20
		mul.s $f25, $f25, $f20 #sexto fator da Série de Taylor (antes da divisão pelo fatorial)
		#add.s $f26, $f1, $f25 #guardo o valor para prosseguir com a exponenciação
		div.s $f25, $f25, $f30 #sexto fator da Série de Taylor
		sub.s $f25, $f1, $f25 #inverter o valor devido a disposição da Série de Taylor
		
		add.s $f5, $f1, $f20 #x
		add.s $f5, $f5, $f21 #x - x³ / 3!
		add.s $f5, $f5, $f22 #x - x³ / 3! + x�?� / 5! 
		add.s $f5, $f5, $f23 #x - x³ / 3! + x�?� / 5! - x^7 / 7!
		add.s $f5, $f5, $f24 #x - x³ / 3! + x�?� / 5! - x^7 / 7! + x^9 / 9!
		add.s $f5, $f5, $f25 #x - x³ / 3! + x�?� / 5! - x^7 / 7! + x^9 / 9! - x^11 / 11!
		beq $t2, 3, troca_sen_quad3
		volta_sen_quad3:
		beq $t2, 4, troca_sen_quad4
		volta_sen_quad4:
		jr $ra
	
	troca_sen_quad3:
		lwc1 $f8, zero
		sub.s $f5 $f8, $f5
		j volta_sen_quad3
	
	troca_sen_quad4:
		lwc1 $f8, zero
		sub.s $f5 $f8, $f5
		j volta_sen_quad4	
																	
																			

	fatorial_cosseno:
				
	addi $t1 , $zero , 1 #atribuo o valor 1 a $t1 - j=1
	addi $t4 , $zero , 1 #atribuo o valor 1 a $t4 - i=1  
	# for ( i =1; i <= n ; i ++)
	for2: 
		slti $t7 , $t4 , 13 #n < 13 fat (12) - seta o registrador $t7 enquanto for menor que 13
		beq $t7 , $zero , conversao_fatorial_cosseno #t7 == false -> fim do for
		mult $t1 , $t4 #resultado em 64 bits ( HI & LO ) - j = j * i ;
		mflo $t1 #move o valor de low register - seleciona 32 bits menos sign
		
		beq $t4, 2, fator2
		volta2:
		beq $t4, 4, fator4
		volta4:
		beq $t4, 6, fator6
		volta6:
		beq $t4, 8, fator8
		volta8:
		beq $t4, 10, fator10
		volta10:
		beq $t4, 12, fator12
		volta12:
				
		addi $t4 , $t4 , 1 #soma-se 1 ao registrador $t4 - i = i + 1
		j for2
	
	
	
	fator2:
		add $s2, $zero, $t1
		j volta2
	
	fator4:
		add $s3, $zero, $t1
		j volta4
	
	fator6:
		add $s4, $zero, $t1
		j volta6
	
	fator8:
		add $s5, $zero, $t1
		j volta8

	fator10:
		add $s6, $zero, $t1
		j volta10
	
	fator12:
		add $s7, $zero, $t1
		j volta12
		
	

		
	conversao_fatorial_cosseno:
	
		mtc1 $s2, $f19 #transfere para $f26
		cvt.s.w $f19, $f19 #converte de inteiro para float
	
		mtc1 $s3, $f20 #transfere para $f26
		cvt.s.w $f20, $f20 #converte de inteiro para float
		
		mtc1 $s4, $f21 #transfere para $f26
		cvt.s.w $f21, $f21 #converte de inteiro para float
		
		mtc1 $s5, $f22 #transfere para $f26
		cvt.s.w $f22, $f22 #converte de inteiro para float
		
		mtc1 $s6, $f23 #transfere para $f26
		cvt.s.w $f23, $f23 #converte de inteiro para float
		
		mtc1 $s7, $f24 #transfere para $f26
		cvt.s.w $f24, $f24 #converte de inteiro para float
		j cosseno	
						
										
	
	cosseno:
	
		lwc1 $f1, zero
		lwc1 $f13, um #primeiro fator da Série de Taylor
		 
		add.s $f11, $f1, $f4
		mul.s $f14, $f11, $f11 #segundo fator da Série de Taylor (antes da divisão pelo fatorial)
		add.s $f15, $f1, $f14 #guardo o valor para prosseguir com a exponenciação
		sub.s $f14, $f1, $f14 #inverter o valor devido a disposição da Série de Taylor
		div.s $f14, $f14, $f19 #segundo fator da Série de Taylor
				
		mul.s $f15, $f15, $f11
		mul.s $f15, $f15, $f11 #terceiro fator da Série de Taylor (antes da divisão pelo fatorial)
		add.s $f16, $f1, $f15 #guardo o valor para prosseguir com a exponenciação
		div.s $f15, $f15, $f20 #terceiro fator da Série de Taylor
		
		mul.s $f16, $f16, $f11
		mul.s $f16, $f16, $f11 #quarto fator da Série de Taylor (antes da divisão pelo fatorial)
		add.s $f17, $f1, $f16 #guardo o valor para prosseguir com a exponenciação
		div.s $f16, $f16, $f21 #quarto fator da Série de Taylor
		sub.s $f16, $f1, $f16 #inverter o valor devido a disposição da Série de Taylor
		
		mul.s $f17, $f17, $f11
		mul.s $f17, $f17, $f11 #quinto fator da Série de Taylor (antes da divisão pelo fatorial)
		add.s $f18, $f1, $f17 #guardo o valor para prosseguir com a exponenciação
		div.s $f17, $f17, $f22 #quinto fator da Série de Taylor
		
		mul.s $f18, $f18, $f11
		mul.s $f18, $f18, $f11 #sexto fator da Série de Taylor (antes da divisão pelo fatorial)
		add.s $f19, $f1, $f18 #guardo o valor para prosseguir com a exponenciação
		div.s $f18, $f18, $f23 #sexto fator da Série de Taylor
		sub.s $f18, $f1, $f18 #inverter o valor devido a disposição da Série de Taylor
		
		mul.s $f19, $f19, $f11
		mul.s $f19, $f19, $f11 #sexto fator da Série de Taylor (antes da divisão pelo fatorial)
		#add.s $f19, $f1, $f18 #guardo o valor para prosseguir com a exponenciação
		div.s $f19, $f19, $f24 #sexto fator da Série de Taylor
				
		add.s $f6, $f1, $f13 #1
		add.s $f6, $f6, $f14 #1 - x² / 2!
		add.s $f6, $f6, $f15 #1 - x² / 2! + x^4 / 4! 
		add.s $f6, $f6, $f16 #1 - x² / 2! + x^4 / 4! - x^6 / 6!
		add.s $f6, $f6, $f17 #1 - x² / 2! + x^4 / 4! - x^6 / 6! + x^8 / 8!
		add.s $f6, $f6, $f18 #1 - x² / 2! + x^4 / 4! - x^6 / 6! + x^8 / 8! - x^10 / 10!
		add.s $f6, $f6, $f19 #1 - x² / 2! + x^4 / 4! - x^6 / 6! + x^8 / 8! - x^10 / 10! + X^12 / 12!
		beq $t2, 2, troca_cos_quad2
		volta_cos_quad2:
		beq $t2, 3, troca_cos_quad3
		volta_cos_quad3:
		jr $ra
		
	troca_cos_quad2:
		lwc1 $f8, zero
		sub.s $f6 $f8, $f6
		j volta_cos_quad2
	
	troca_cos_quad3:
		lwc1 $f8, zero
		sub.s $f6 $f8, $f6
		j volta_cos_quad3	
	
			
	rotaciona:
		
		addi $t0, $zero, 0
		addi $t1, $zero, 1
		
		#VALORES DE COSSENO
		lw $s0, myArray($t0)
		mtc1 $s0, $f9 #transfere para $f9
		cvt.s.w $f9, $f9 #converte de inteiro para float
		mul.s $f9, $f6, $f9 #cosseno x primeiro argumento do vetor
		sll $t0, $t1, 2
		
		#VALORES DE SENO
		lw $1, myArray($t0)
		mtc1 $s1, $f8 #transfere para $f8
		cvt.s.w $f8, $f8 #converte de inteiro para float
		lwc1 $f7, zero #atribuindo zero ao registrador para subtraí-lo
		sub.s $f7, $f7, $f5 #invertendo o sinal do seno
		mul.s $f8, $f8, $f7 #-seno x segundo argumento do vetor
		
		add.s $f12, $f8, $f9 #primeiro termo do vetor resultante
		
		li $v0, 4 #impressão de espaço
		la $a0, space
		syscall
		li $v0, 4 #impressão do texto de saída
		la $a0, msg2
		syscall
		li $v0, 2 #impressão do vetor resposta
		syscall
		li $v0, 4 #impressão de espaço
		la $a0, space
		syscall
		
						
		#VALORES DE SENO
		mtc1 $s0, $f10 #transfere para $f10
		cvt.s.w $f10, $f10 #converte de inteiro para float
		mul.s $f10, $f5, $f10 #seno x primeiro argumento do vetor
				
		#VALORES DE COSSENO
		mtc1 $s1, $f11 #transfere para $f11
		cvt.s.w $f11, $f11 #converte de inteiro para float
		mul.s $f11, $f6, $f11 #cosseno x segundo argumento do vetor
		
		add.s $f12, $f10, $f11 #primeiro termo do vetor resultante
		
		li $v0, 4 #impressão de espaço
		la $a0, space
		syscall
		li $v0, 2 #impressão do vetor resposta
		syscall
		li $v0, 4 #término da impressão do texto de saída
		la $a0, msg3
		syscall
	
		li $v0, 10
		syscall
	
